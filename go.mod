module gitlab.com/datdang_5492/grpc_play_ground

go 1.12

require (
	cloud.google.com/go v0.58.0 // indirect
	cloud.google.com/go/pubsub v1.4.0 // indirect
	cloud.google.com/go/storage v1.9.0 // indirect
	dmitri.shuralyov.com/gpu/mtl v0.0.0-20191203043605-d42048ed14fd // indirect
	github.com/BurntSushi/xgb v0.0.0-20200324125942-20f126ea2843 // indirect
	github.com/cncf/udpa/go v0.0.0-20200508205342-3b31d022a144 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/envoyproxy/go-control-plane v0.9.5 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200420212212-258d9bec320e // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/pprof v0.0.0-20200604032702-163a225fb653 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20200524003926-2c5affb30a03 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.6.0 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/yuin/goldmark v1.1.32 // indirect
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
	golang.org/x/exp v0.0.0-20200513190911-00229845015e // indirect
	golang.org/x/image v0.0.0-20200609002522-3f4726a040e8 // indirect
	golang.org/x/mobile v0.0.0-20200329125638-4c31acba0007 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	golang.org/x/tools v0.0.0-20200612220849-54c614fe050c // indirect
	google.golang.org/genproto v0.0.0-20200612171551-7676ae05be11 // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.24.0
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
	rsc.io/sampler v1.99.99 // indirect
)
