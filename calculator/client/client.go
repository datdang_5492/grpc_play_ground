package main

import (
	"context"
	"gitlab.com/datdang_5492/grpc_play_ground/calculator/calculatorpb"
	"google.golang.org/grpc"
	"log"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect %v", err)
	}
	defer conn.Close()

	c := calculatorpb.NewCalculatorServiceClient(conn)

	req := &calculatorpb.SumRequest{
		FirstNo:  12,
		SecondNo: 21,
	}

	res, err := c.Sum(context.Background(), req)
	if err != nil {
		log.Fatalf("error when sending sum request: %v", err)
	}

	log.Printf("Response showing SUM: %v", res.Sum)
}
